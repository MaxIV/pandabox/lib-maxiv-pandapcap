import typing


class AcquisitionConfig(typing.NamedTuple):
    destination_filename: str
    channel_renaming: dict
    channel_formula: dict
