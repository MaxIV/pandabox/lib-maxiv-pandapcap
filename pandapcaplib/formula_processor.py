from pandablocks.hdf import FrameProcessor
from pandablocks.responses import FieldCapture
from pandapcaplib.evaluate_formula import EvaluateFormula
import numpy as np


class FormulaFrameProcessor(FrameProcessor):
    """Inherits from FrameProcessor in PandaBlock-client.
    Adds processor for evaluating formula."""

    def __init__(self, dataset_formula: dict[str, EvaluateFormula] = {}):
        super().__init__()
        self.dataset_formula = dict((k.lower(), v) for k, v in dataset_formula.items())

    def _formula_processor(
        self, formula: EvaluateFormula, data: np.ndarray
    ) -> np.ndarray:
        """Replace data values with evaluated values."""
        for i, d in enumerate(data):
            data[i] = formula.evaluate(d)
        return data

    def create_processor(self, field: FieldCapture, raw: bool):
        """Overridden to add formula processor."""
        column_name = f"{field.name}.{field.capture}"
        formula = self.dataset_formula.get(column_name.lower(), None)
        if isinstance(formula, EvaluateFormula):
            return lambda data: self._formula_processor(formula, data[column_name])
        else:
            return lambda data: data[column_name]
