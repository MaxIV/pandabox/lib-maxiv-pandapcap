from pathlib import Path


class PathValidationException(Exception):
    pass


class PathValidator:
    def __init__(self):
        """
        __init__
        """
        pass

    def validate(self, filepath: str):
        """
        validate will consider the given path and respond with a True value if
        ok otherwise raise an exception.
        """
        # Make sure string is not empty
        filepath = filepath.strip()
        if filepath == "":
            raise PathValidationException("path cannot be empty")

        filepath = Path(filepath)

        # Make sure it got .h5 in the end
        if filepath.suffix != ".h5":
            raise PathValidationException("path must have .h5 in the end")

        # Make sure it's absolut
        if not filepath.is_absolute():
            raise PathValidationException("path must be absolute")

        # Backslash should not be allowed
        if "\\" in str(filepath):
            raise PathValidationException("path cannot contain backslash")

        # Refer to parent
        parent_dir = filepath.parent

        # For some machines "/" is read-only
        if str(parent_dir) == "/":
            raise PathValidationException("path cannot be at /")

        # Check if path exists
        #
        # We do it like this instead of filepath.exists() because GPFS can be
        # slow and miss a file that is there, until you 'list' the directory,
        # so that's what we're doing here.
        exists = False
        if parent_dir.exists():
            for item in parent_dir.iterdir():
                if item == filepath:
                    exists = True
                    break

        if exists:
            raise PathValidationException(f"file {filepath} already exists")

        # Create parent directory
        try:
            parent_dir.mkdir(parents=True, exist_ok=True)

        except OSError as e:
            raise PathValidationException(
                f"could not create directory {parent_dir}: {str(e)}"
            )

        # Create file and write to it
        try:
            # append file name
            sample_path = parent_dir / "daq-sample-file.txt"

            # write file
            with open(sample_path, "w") as tmp_file:
                tmp_file.write("sample-data")

            # remove file
            sample_path.unlink()
        except OSError as e:
            raise PathValidationException(
                f"could not create file at {sample_path}: {str(e)}"
            )

        return True
