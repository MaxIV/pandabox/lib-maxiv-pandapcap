import logging
import h5py
import numpy as np

from pandablocks.hdf import HDFWriter
from pandablocks.responses import FieldCapture, StartData


# Define the public API of this module
__all__ = [
    "NexusHDFWriter",
]

ENTRY = "entry"
INSTRUMENT = "instrument"
DETECTOR = "pandabox"


class NexusHDFWriter(HDFWriter):
    """Inherites from the HDFWriter from PandaBlock-client.
    Adds Nexus formatting and possibility to rename datasets.
    """

    def __init__(self, file_names: iter([str]), dataset_renaming: dict = {}):
        super().__init__(file_names)
        self._internal_path = f"{ENTRY}/{INSTRUMENT}/{DETECTOR}/data"
        self._internal_data_path = self._internal_path + "/data"
        self._dataset_renaming = dict(
            (k.lower(), v) for k, v in dataset_renaming.items()
        )

    def create_dataset(self, field: FieldCapture, raw: bool):
        """Overridden to add nexus groups"""
        assert self.hdf_file, "File not open yet"
        # Data written in a big stack, growing in that dimension
        if raw and (field.capture == "Mean" or field.scale != 1 or field.offset != 0):
            # Processor outputs a float
            dtype = np.dtype("float64")
        else:
            # No processor, datatype passed through
            dtype = field.type
        # Rename datasets
        name = self._dataset_renaming.get(
            f"{field.name.lower()}.{field.capture.lower()}",
            f"{field.name}.{field.capture}",
        )
        return self.hdf_file.create_dataset(
            f"{self._internal_path}/{name}",
            dtype=dtype,
            shape=(0,),
            maxshape=(None,),
        )

    def open_file(self, data: StartData):
        """Overridden to add nexus groups"""
        try:
            self.file_path = next(self.file_names)
        except IndexError:
            logging.exception(
                "Not enough file names available when opening new HDF5 file"
            )
            raise
        self.hdf_file = h5py.File(self.file_path, "w", libver="latest")
        # Add nexus groups to file
        self.hdf_file.require_group(ENTRY).attrs["NX_class"] = "NXentry"
        self.hdf_file.require_group(f"{ENTRY}/{INSTRUMENT}").attrs[
            "NX_class"
        ] = "NXinstrument"
        self.hdf_file.require_group(self._internal_path).attrs[
            "NX_class"
        ] = "NXdetector"
        raw = data.process == "Raw"
        self.datasets = [self.create_dataset(field, raw) for field in data.fields]
        self.hdf_file.swmr_mode = True
        logging.info(
            f"Opened '{self.file_path}' with {data.sample_bytes} byte samples "
            f"stored in {len(self.datasets)} datasets"
        )
