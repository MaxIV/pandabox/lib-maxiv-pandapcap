from pandablocks.hdf import Pipeline
from pandablocks.responses import StartData, EndData
import numpy as np
import zmq
import time


REPUBLISHER_PORT = 22010
VERSION = 1


class Republisher(Pipeline):
    """Republishes the data"""

    def __init__(
        self, republisher_port: int = REPUBLISHER_PORT, dataset_renaming: dict = {}
    ):
        super().__init__()

        self._port = republisher_port
        self._republisher = None
        self._headers = list()
        self._message_id = 1
        self._frame_number = 0
        self._version = VERSION

        self._dataset_renaming = dict(
            (k.lower(), v) for k, v in dataset_renaming.items()
        )

        self.what_to_do = {
            StartData: self.open_republisher,
            list: self.republish,
            EndData: self.close_republisher,
        }

    def open_republisher(self, data: StartData) -> StartData:
        if self._republisher:
            self._republisher.close()
            self._republisher = None
            self._headers = list()
            self._message_id = 1
            self._frame_number = 0

        if self._port:
            self._context = zmq.Context()
            self._republisher = self._context.socket(zmq.PUB)
            self._republisher.bind(f"tcp://*:{self._port}")

            for field in data.fields:
                self._headers.append(
                    self._dataset_renaming.get(
                        f"{field.name.lower()}.{field.capture.lower()}",
                        f"{field.name}.{field.capture}",
                    )
                )

            message = {
                "message_id": self._message_id,
                "message_type": "series-start",
                "version": self._version,
            }

            # TODO: Fix this. Without sleep first message is not sent, investigate???
            time.sleep(0.1)
            self._republisher.send_json(message)

            self._message_id += 1

        return data

    def republish(self, data: list[np.ndarray]) -> list[np.ndarray]:
        if self._republisher:
            message = {
                "message_id": self._message_id,
                "message_type": "data",
                "version": self._version,
                "frame_number": self._frame_number,
            }

            for header, value in zip(self._headers, data):
                message[header] = list(value)

            self._republisher.send_json(message)

            self._message_id += 1
            self._frame_number += len(data[0].data)

        return data

    def close_republisher(self, data: EndData = None) -> EndData:
        if self._republisher:
            message = {
                "message_id": self._message_id,
                "message_type": "series-end",
                "version": self._version,
            }

            self._republisher.send_json(message)

            self._republisher.close()
            self._republisher = None
            self._headers = list()
            self._message_id = 1
            self._frame_number = 0

        return data
