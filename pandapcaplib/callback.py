from pandablocks.hdf import Pipeline
from pandablocks.responses import StartData
import numpy as np


class DataCallback(Pipeline):
    """Sends headers and processed data to caller"""

    def __init__(self, callback: callable = None, dataset_renaming: dict = {}):
        super().__init__()
        self.callback = callback
        self._dataset_renaming = dict(
            (k.lower(), v) for k, v in dataset_renaming.items()
        )
        self.what_to_do = {
            StartData: self.send_headers_to_caller,
            list: self.send_data_to_caller,
        }

    def send_headers_to_caller(self, data: StartData) -> StartData:
        if callable(self.callback):
            headers = []
            for field in data.fields:
                headers.append(
                    self._dataset_renaming.get(
                        f"{field.name.lower()}.{field.capture.lower()}",
                        f"{field.name}.{field.capture}",
                    )
                )
            self.callback(headers)
        return data

    def send_data_to_caller(self, data: list[np.ndarray]) -> list[np.ndarray]:
        if callable(self.callback):
            self.callback(data)
        return data
