#!/usr/bin/env python
import threading
import logging
from functools import wraps
from socket import gethostbyname, gethostname

from pandablocks.blocking import BlockingClient
from pandablocks.commands import Get, Arm, Disarm
from pandablocks.hdf import create_pipeline, stop_pipeline
from pandablocks.responses import ReadyData, FrameData, EndData

from pandapcaplib.nexus_writer import NexusHDFWriter
from pandapcaplib.path_validator import PathValidator
from pandapcaplib.acquisition_config import AcquisitionConfig
from pandapcaplib.callback import DataCallback
from pandapcaplib.formula_processor import FormulaFrameProcessor
from pandapcaplib.evaluate_formula import EvaluateFormula
from pandapcaplib.republisher import Republisher
from pandapcaplib._version import version


logger = logging.getLogger(__name__)


def debug_log(func):
    """Debug log decorator"""

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            logger.debug(f"-> Entering {func.__name__} with args: {args}")
            output = func(self, *args, **kwargs)
            logger.debug(f"<- Exiting {func.__name__} with output: {output}")
            return output
        except Exception as e:
            logger.error(str(e))
            raise e

    return wrapper


class PandaPcapClient(object):
    """
    Wrapper class for the PandABlocks-client
    """

    def __init__(
        self,
        host: str = "localhost",
        timeout_ctrl: int = None,
        timeout_data: int = None,
        republisher_port: int = None,
    ):
        self._client = BlockingClient(host)
        self._timeout_ctrl = timeout_ctrl
        self._timeout_data = timeout_data
        self._path_validator = PathValidator()
        self._thread = None
        self._thread_error = None
        self._num_samples_acquired = 0
        self._republisher_port = republisher_port

    @debug_log
    def connect(self):
        self._client.connect()

    @debug_log
    def close(self):
        self._client.close()

    @property
    def lib_version(self):
        return version

    @property
    def fw_version(self):
        idn = self._client.send(Get("*IDN"), self._timeout_ctrl)
        return idn

    @property
    def is_busy(self):
        state = self._client.send(Get("*PCAP.STATUS"), self._timeout_ctrl)
        if "Busy" in state:
            return True
        else:
            return False

    @property
    def is_running(self):
        if self._thread is None:
            return False
        return self._thread.is_alive()

    @property
    def num_samples_acquired(self):
        return self._num_samples_acquired

    @property
    def zmq_republisher_addr(self):
        if self._republisher_port:
            return f"tcp://{gethostbyname(gethostname())}:{self._republisher_port}"
        else:
            return ""

    def check_error(self):
        if self._thread_error:
            err_msg = f"Error in acquisition loop. Exception: {self._thread_error}"
            raise RuntimeError(err_msg)

    @debug_log
    def arm(self, config: AcquisitionConfig, callback: callable = None):
        # Validate path
        self._path_validator.validate(config.destination_filename)

        # Create and validate formula object from string
        ch_formula = {}
        for k, v in config.channel_formula.items():
            ef = EvaluateFormula(v)
            ch_formula[k] = ef

        # Start the acquisition and HDF file writer thread
        self._thread = threading.Thread(
            target=self._run,
            args=(
                config.destination_filename,
                config.channel_renaming,
                ch_formula,
                callback,
            ),
        )
        self._thread.start()

    @debug_log
    def disarm(self):
        # always allow command Disarm to be sent
        self._client.send(Disarm(), self._timeout_ctrl)
        if self._thread:
            self._thread.join()
        self._thread = None

    # Acquisition loop
    @debug_log
    def _run(
        self,
        filename: str,
        dataset_renaming: dict = {},
        dataset_formula: dict = {},
        callback: callable = None,
    ):
        """Connect to data port and read captured data, send it through pipeline.
        Process the data (evaluate formula), send to caller and write to HDF file."""
        self._thread_error = None
        pipeline = create_pipeline(
            FormulaFrameProcessor(dataset_formula),
            DataCallback(callback, dataset_renaming),
            Republisher(self._republisher_port, dataset_renaming),
            NexusHDFWriter(iter([filename]), dataset_renaming),
        )
        try:
            self._num_samples_acquired = 0
            for data in self._client.data(
                scaled=True, frame_timeout=self._timeout_data
            ):
                pipeline[0].queue.put_nowait(data)
                if isinstance(data, ReadyData):
                    # Told to arm when data is ready
                    self._client.send(Arm(), self._timeout_ctrl)
                elif isinstance(data, FrameData):
                    # Got some frame data, increase sample counter
                    self._num_samples_acquired += len(data.data)
                elif isinstance(data, EndData):
                    break
        except Exception as e:
            self._thread_error = e
            try:
                # Disarm to be safe
                self._client.send(Disarm(), 0)
            except Exception:
                pass
        finally:
            stop_pipeline(pipeline)
