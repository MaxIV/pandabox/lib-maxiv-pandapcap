import math

MATH_FUNCTIONS = {k: v for k, v in math.__dict__.items() if not k.startswith("__")}


class EvaluateFormulaException(Exception):
    pass


class EvaluateFormula:
    """Class for evaluating formulas with some added security checks.
    Only allows certain functions to be evaluated."""

    def __init__(self, formula: str):
        """Compile formula and check if valid."""
        self._allowed_names = {**MATH_FUNCTIONS, **{"value": None}}
        self._code = None
        self._formula = formula.strip().lower()

        if self._formula == "":
            raise EvaluateFormulaException("Formula cannot be empty")

        self._code = compile(self._formula, "<string>", "eval")
        for name in self._code.co_names:
            if name not in self._allowed_names:
                self._code = None
                raise EvaluateFormulaException(f"Formula name '{name}' is not allowed")

        try:
            self.evaluate(1)
        except ValueError:
            pass  # Not possible to predict
        except Exception as e:
            raise EvaluateFormulaException(f"Formula '{formula}' invalid, {e}")

    def evaluate(self, value):
        """Evaluate compiled formula."""
        if self._code is not None:
            self._allowed_names["value"] = value
            eval_value = eval(self._code, {"__builtins__": {}}, self._allowed_names)
            return eval_value
        else:
            return value


if __name__ == "__main__":
    f = "sqrt(value)*pow(value,2)/(value+value)"
    v = 25
    ef = EvaluateFormula(f)
    print("formula:", f)
    print("value:", v)
    print("result:", ef.evaluate(v))
