#!/usr/bin/env python
import logging
import time
import sys
import os

from pandapcaplib.pandapcap_client import PandaPcapClient
from pandapcaplib.acquisition_config import AcquisitionConfig

logging.basicConfig(level=logging.DEBUG)

filename = "/tmp/test-pcap-01.h5"
channel_renaming = {
    "COUNTER2.OUT": "SomeOtherName",
}
channel_formula = {
    "COUNTER2.OUT": "pow(value,2)",
}

config = AcquisitionConfig(
    destination_filename=filename,
    channel_renaming=channel_renaming,
    channel_formula=channel_formula,
)

# Remove any previous file
try:
    os.remove(filename)
except FileNotFoundError:
    pass


# Callback function
def print_data_cb(data):
    if isinstance(data[0], str):
        print("Headers: ", data)
    else:
        print("Data: ", data)


# Set host as argument
panda = PandaPcapClient(sys.argv[1])
panda.connect()
print(panda.fw_version)
panda.arm(config, print_data_cb)
while panda.is_running:
    try:
        time.sleep(0.1)
    except KeyboardInterrupt:
        print("Exit by user")
        panda.disarm()
panda.check_error()
panda.close()
