#!/usr/bin/env python
import socket
from threading import Thread, Event
from queue import Queue, Empty
import time

"""
Dummy server simulating the Pandabox PCAP
"""

busy = False


class SocketServer(Thread):
    """Base class for handling a socket connection"""

    def __init__(self, host="localhost", port=0):
        Thread.__init__(self)
        self._conn = None
        self._event = Event()
        self._lf = "\n"
        self._verbose = True

        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Set option to avoid "[Errno 98] Address already in use"
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.setblocking(0)
        self._sock.settimeout(1)
        self._log("Socket created")
        # Bind socket to local host and port
        try:
            self._sock.bind((host, port))
        except socket.error as e:
            self._log("Socket bind failed!")
            self._log(e)
            self._sock.close()
        self._log("Socket bind complete")
        # Start listening on socket
        self._sock.listen(1)
        self._log("Socket listening")

    def _log(self, msg):
        print(f"{self._name} - {msg}")

    def send(self, data):
        snd = (data + self._lf).encode()
        if self._verbose:
            self._log(f"SEND: {snd}")
        self._conn.sendall(snd)

    def send_bin(self, data):
        snd = data
        if self._verbose:
            self._log(f"SEND: {snd}")
        self._conn.sendall(snd)

    def recv(self):
        # Receive one byte at the time until term char detected
        rec = ""
        while True:
            b = self._conn.recv(1).decode()
            if b:
                if b != self._lf:
                    rec += b
                else:
                    break
            else:
                raise RuntimeError("Connection lost!")
        if self._verbose:
            self._log(f"RECV: {rec}")
        return rec

    def close(self):
        if self._conn:
            self._conn.close()
        if self._sock:
            self._sock.close()

    def _pre_run(self):
        pass

    def _process_data(self):
        raise NotImplementedError()

    def _post_run(self):
        pass

    def run(self):
        # Main loop
        while not self._event.is_set():
            self._pre_run()
            try:
                if self._conn is None:
                    # Wait for new connection from client
                    self._log("Socket wait for client connection...")
                    self._conn, addr = self._sock.accept()
                    self._conn.settimeout(0.1)
                    self._log(f"Socket connected to {str(addr[0])}")
                else:
                    # Handle connection until it is closed
                    self._process_data(self.recv().upper())
            except socket.error:  # as e:
                # self._log(f"Socket error: {str(e)}")
                pass
            except Exception as e:
                self._conn = None
                self._log(e)
            self._post_run()

    def stop(self):
        self._event.set()
        self._conn = None
        self._sock = None


class ControlServer(SocketServer):
    """Subclass for pcap control"""

    def __init__(self, host: str, q_out: Queue):
        SocketServer.__init__(self, host, 8888)
        self._name = "Control"
        self._q_out = q_out

    def _process_data(self, data):
        global busy
        if data:
            if "*IDN?" in data:
                self.send("OK =PandA SW: 330bd94-dirty FPGA: 0.1.9 d1275f61 00000000")
            # elif "*BLOCKS?" in data:
            #     self.send(pcap_blocks)
            elif "*PCAP.ARM=" in data:
                busy = True
                self._q_out.put(data)
                self.send("OK")
            elif "*PCAP.DISARM=" in data:
                busy = False
                self._q_out.put(data)
                self.send("OK")
            elif "SEQ1.REPEATS=" in data:
                self._q_out.put(data)
                self.send("OK")
            elif "SEQ1.PRESCALE=" in data:
                self.send("OK")
            elif "SEQ1.PRESCALE.UNITS=" in data:
                self.send("OK")
            elif "*PCAP.STATUS?" in data:
                if busy:
                    self.send("OK =Busy")
                else:
                    self.send("OK =Idle")
            else:
                self.send("ERR Unknown command")
                self._log("Command not found!")


class DataServer(SocketServer):
    """Subclass for pcap data"""

    DELAY_AFTER_DATA_FRAME = 0.1
    DATA_FRAME_THREADING_EVENT_TIMEOUT = 1.0

    def __init__(self, host: str, q_in: Queue):
        SocketServer.__init__(self, host, 8889)
        self._name = "Data"
        self._q_in = q_in
        self._repeats = 0
        self._pcap_data = None
        self._stop_data = False
        self.data_frame_sent = Event()
        self.next_data_frame_allowed = Event()
        self.next_data_frame_allowed.set()
        self.end_frame_sent = Event()
        self.worst_case_send_time = 0.0

    def load_data(self, file):
        with open(file, "rb") as f:
            self._pcap_data = f.read()

    def _process_data(self, data):
        if data:
            if "XML " in data:
                self.send("OK")
            else:
                self.send("ERR Unknown command")
                self._log("Command not found!")

    def _post_run(self):
        try:
            data = self._q_in.get(False).upper()
            self._log(f"Q: {data}")
            if "SEQ1.REPEATS=" in data:
                self._repeats = int(data.split("=")[1])
            elif "*PCAP.ARM=" in data:
                self._stop_data = False
                x = Thread(target=self._send_data)
                x.start()
            elif "*PCAP.DISARM=" in data:
                self._stop_data = True
            else:
                self._log("Command not found!")
        except Empty:
            pass

    def _send_data(self):
        global busy
        del_bin = b"BIN "
        del_end = b"END "
        header_data = self._pcap_data.split(del_bin)[0]
        bin_data = self._pcap_data.split(del_end)[0].split(del_bin)[1:]
        end_data = self._pcap_data.split(del_end)[1]
        self.worst_case_send_time = self.DELAY_AFTER_DATA_FRAME * len(bin_data) * 1.5

        self.send_bin(header_data)
        for d in bin_data:
            if self._stop_data or self._event.is_set():
                break
            self.send_bin(del_bin + d)
            time.sleep(self.DELAY_AFTER_DATA_FRAME)
            self.data_frame_sent.set()
            self.next_data_frame_allowed.wait(self.DATA_FRAME_THREADING_EVENT_TIMEOUT)
        self.send_bin(del_end + end_data)
        busy = False
        self.end_frame_sent.set()


def main():

    from pathlib import Path

    print("Started")

    host = "localhost"
    q = Queue(10)

    server_ctrl = ControlServer(host, q)
    server_data = DataServer(host, q)
    # Load fake pcap data from file
    server_data.load_data(f"{Path(__file__).parent}/fast_dump.txt")

    server_ctrl.start()
    server_data.start()

    while True:
        try:
            time.sleep(0.5)
        except KeyboardInterrupt:
            print("Exit by user")
            server_ctrl.stop()
            server_data.stop()
            break

    server_ctrl.join()
    server_data.join()

    server_ctrl.close()
    server_data.close()

    print("Exited")


if __name__ == "__main__":
    main()
