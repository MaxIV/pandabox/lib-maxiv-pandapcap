import pytest
import numpy as np
import time

from pandapcaplib.pandapcap_client import PandaPcapClient
from pandapcaplib.acquisition_config import AcquisitionConfig
from pandapcaplib import __version__ as lib_version  # noqa: E402


### Constants ###

config = AcquisitionConfig(
    destination_filename=pytest.filename,
    channel_renaming={
        "COUNTER2.OUT.Mean": "SomeOtherName",
    },
    channel_formula={
        "COUNTER2.OUT.Mean": "pow(value,2)",
    },
)
fw = "PandA SW: 330bd94-dirty FPGA: 0.1.9 d1275f61 00000000"

# Expected data captured after channel renaming and formula evaluated
expected_last_captured_data = {
    "PCAP.BITS2.Value": 8.0,
    "COUNTER1.OUT.Min": 58.0,
    "COUNTER1.OUT.Max": 58.0,
    "COUNTER3.OUT.Value": 174.0,
    "PCAP.TS_START.Value": 0.570000056,
    "COUNTER1.OUT.Mean": 58.0,
    "SomeOtherName": 13456.0,
}
expected_num_samples_acquired_first_frame = 10
expected_num_samples_acquired_total = 58

delay_for_client_thread_action = 0.2
timeout_for_data_frame = 0.5

### Variables ###

last_captured_data = {}


### Callback function ###


def data_callback(data):
    if isinstance(data[0], str):  # Headers
        for ch_name in data:
            last_captured_data[ch_name] = None
    elif isinstance(data[0], np.ndarray):  # Values
        # Replace with last captured value for each channel
        last_values = [float(val[-1]) for val in data]
        for (name, _), value in zip(last_captured_data.items(), last_values):
            last_captured_data[name] = value


### Tests ###


def test_connection(dummy_servers):
    # Connection OK
    client = PandaPcapClient("localhost")
    client.connect()
    assert client.lib_version == lib_version
    assert client.fw_version == fw
    client.check_error()
    client.close()

    # Connection FAIL
    client = PandaPcapClient("invalid_address")
    with pytest.raises(OSError, match="not known"):
        client.connect()


def test_client_initial_state(dummy_servers, pcap_client):
    client = pcap_client

    client.check_error()
    assert not client.is_running
    assert not client.is_busy
    assert client.num_samples_acquired == 0
    assert not pytest.testfile_exists()


def test_client_data_capture_completed(dummy_servers, pcap_client):
    client = pcap_client
    _, data_server = dummy_servers

    client.arm(config, data_callback)
    time.sleep(delay_for_client_thread_action)
    assert client.is_running
    assert client.is_busy
    assert data_server.end_frame_sent.wait(data_server.worst_case_send_time)
    time.sleep(delay_for_client_thread_action)
    assert not client.is_running
    assert not client.is_busy
    assert client.num_samples_acquired == expected_num_samples_acquired_total
    assert last_captured_data == expected_last_captured_data
    assert pytest.testfile_exists()
    pytest.testfile_remove()
    client.check_error()


def test_client_data_capture_interrupted(dummy_servers, pcap_client):
    client = pcap_client
    _, data_server = dummy_servers

    client.arm(config, data_callback)
    time.sleep(delay_for_client_thread_action)
    assert client.is_running
    assert client.is_busy
    assert data_server.data_frame_sent.wait(timeout_for_data_frame)
    client.disarm()  # stop capture before end of data
    time.sleep(delay_for_client_thread_action)
    assert not client.is_running
    assert not client.is_busy
    assert last_captured_data != expected_last_captured_data
    assert 0 < client.num_samples_acquired < expected_num_samples_acquired_total
    assert pytest.testfile_exists()
    pytest.testfile_remove()
    client.check_error()


def test_client_num_samples_acquired_updated_immediately(dummy_servers, pcap_client):
    client = pcap_client
    _, data_server = dummy_servers

    data_server.next_data_frame_allowed.clear()  # block sender after first frame
    client.arm(config, data_callback)
    time.sleep(delay_for_client_thread_action)
    assert client.is_running
    assert client.is_busy
    data_server.data_frame_sent.wait(timeout_for_data_frame)
    assert client.num_samples_acquired == expected_num_samples_acquired_first_frame
    data_server.next_data_frame_allowed.set()
    client.disarm()
    time.sleep(delay_for_client_thread_action)
    assert not client.is_running
    assert not client.is_busy
    assert pytest.testfile_exists()
    pytest.testfile_remove()
