import pytest
from pathlib import Path
from queue import Queue
import os
import time

from dummy.dummy_server import ControlServer, DataServer
from pandapcaplib.pandapcap_client import PandaPcapClient


### Constants ###

host = "localhost"
filename = "/tmp/test-pcap-01.h5"


### Helpers ###


def testfile_exists():
    return os.path.exists(filename)


def testfile_remove():
    if os.path.exists(filename):
        os.remove(filename)
        time.sleep(0.1)


# Make constants accessable from tests
def pytest_configure():
    pytest.filename = filename
    pytest.testfile_exists = testfile_exists
    pytest.testfile_remove = testfile_remove


### Fixtures ###


@pytest.fixture(scope="session")
def dummy_servers():
    q = Queue(10)
    server_ctrl = ControlServer(host, q)
    server_data = DataServer(host, q)
    # Load fake pcap data from file
    server_data.load_data(f"{Path(__file__).parent}/dummy/fast_dump.txt")
    server_ctrl.start()
    server_data.start()
    yield server_ctrl, server_data
    server_ctrl.stop()
    server_data.stop()
    server_ctrl.join()
    server_data.join()
    server_ctrl.close()
    server_data.close()


@pytest.fixture()
def pcap_client():
    testfile_remove()
    client = PandaPcapClient(host)
    client.connect()
    yield client
    client.check_error()
    client.close()
    testfile_remove()
