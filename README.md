# pandapcaplib

<img align="center" src="https://nox.apps.okd.maxiv.lu.se/widget?conda_package=pandapcaplib&rpm_package=python36-pandapcaplib"/>

Python module for communicating with the PandaBox PCAP block and storing captured data to file.

This module wraps the communication interface and HDF writer functionality from the [PandABlocks-client](https://github.com/PandABlocks/PandABlocks-client) with some modification for storing the files in Nexus format. This module also adds functionality to readout data via callback, enable renaming channel names and evaluating formulas for channels.

## Example

See `scripts/example.py` for an example on using the library.

A connection is made to the pandabox on specified host. When connected, an arm command is sent to start capturing data. The data is received and printed as specified in the provided callback funtion. Data is read until end of data is received. This occurrs either when all samples have been read of the user sends a disarm command. The connection to the pandabox is closed.


Run the example and connect to the dummy server on host localhost:
```
python scripts/example.py localhost
```

## Dummy server
A dummy server can be used for generating fake data read from a file via socket. Prefferably run the dummy server in a separate terminal.

Run the dummy server:
```
python tests/dummy/dummy_server.py
```

To stop the dummy server, press ctrl-C.


## Testing

Test requirements:
```
 - pytest
 - pytest-cov
```

Run the test:
```
python -m pytest -sv --cov=pandapcaplib tests/test_pandapcap.py
```